# Gradle for Kotlin to JavaScript Barebone Project

This project is meant to help others like myself who could not easily find example on how to set up a Kotlin to JavaScript project.

### Random tips.

* ./gradlew build

This will build and have the artifacts ready to be used in ./web directory.

* ./gradle -t build

This will monitor file changes and recompile whenever necessary. This is very handy for development.

### How do I get set up? ###

* Place your code in src/main/kotlin.
* Images in src/main/webapp/image.
* JS libs in src/main/webapp/lib.
* I tried my best to place files in what I guess is the standard place but since this is border of gradle/kotlin/js worlds, I may have guessed wrong. Do share with me your opinions.
* How to run tests? Not setup yet. Do help out.

### Contribution guidelines ###

* Just share your idea or pull requests.

### Who do I talk to? ###

* Me?