requirejs.config({
    paths: {
        kotlin: 'kotlin',
        snapsvg: 'lib/snap.svg',
        output: 'output'
    }
});

requirejs(["snapsvg", "output"], function (snapsvg, output) {
    var sample = new output.Sample(snapsvg);
    sample.draw();
});
