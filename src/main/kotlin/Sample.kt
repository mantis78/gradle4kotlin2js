import Snap.Element
import WorkStatus.*
import kotlin.browser.window
import kotlin.js.Math
import kotlin.js.json

class Sample(val snapInitFunc: (String) -> Snap.Paper) {
    val s: Snap.Paper = snapInitFunc("#svg")
    var cursor = Cursor(Coord(0, 0))

    fun draw() {
        val lineAttr = json("strokeWidth" to 5)
        val internetAttr = json("stroke" to "blue").add(lineAttr)
        val intranetAttr = json("stroke" to "darkgreen").add(lineAttr)
        val securityAttr = json("stroke" to "black").add(lineAttr)
        val databaseAttr = json("stroke" to "red", "strokeWidth" to 20, "opacity" to 0.2)

        s.line(cursor.x + 200, cursor.y + 100, cursor.x + 300, cursor.y + 250).attr(internetAttr)
        drawModule("Some Project", 200, listOf(WorkItem("Setup", NEW, "boon", 3522),
                WorkItem("Security Config", NEW, "john", 3550),
                WorkItem("Other", IN_PROGRESS, "john", 3551)))
    }

    fun drawArrow(start: Coord, end: Coord): Element {
        val mainLine = s.line(start.x, start.y, end.x, end.y).attr(json("stroke" to "black"))
        val arrowHeadSize = Math.sqrt(((start.x - end.x) * (start.x - end.x) + (start.y - end.y) * (start.y - end.y)).toDouble()) * .2
        val arrowLeft = s.line(end.x, end.y, end.x - arrowHeadSize, end.y + arrowHeadSize).attr(json("stroke" to "black"))
        val arrowRight = s.line(end.x, end.y, end.x + arrowHeadSize, end.y + arrowHeadSize).attr(json("stroke" to "black"))

        return s.g(mainLine, arrowLeft, arrowRight)
    }

    fun drawBox(topLeft: Coord, size: Int): Element {
        val box = s.polygon(arrayOf(topLeft.x, topLeft.y, topLeft.x + size, topLeft.y, topLeft.x + size, topLeft.y + size,
                topLeft.x, topLeft.y + size, topLeft.x, topLeft.y))
        box.attr(json("fill" to "none", "stroke" to "black"))

        return box
    }

    fun drawRect(topLeft: Coord, width: Int, height: Int, color: String): Element {
        val rect = s.polygon(arrayOf(topLeft.x, topLeft.y, topLeft.x + width, topLeft.y, topLeft.x + width, topLeft.y + height,
                topLeft.x, topLeft.y + height, topLeft.x, topLeft.y))
        rect.attr(json("fill" to color, "stroke" to "black"))

        return rect
    }

    fun drawModule(name: String, size: Int, items: List<WorkItem>? = null) {
        var cursor = Cursor(this.cursor.coord)
        drawBox(cursor.coord, size)

        cursor.moveBy(size / 15, size / 8)
        s.text(cursor.x, cursor.y, name).attr(json("font-size" to "${size / 12}px"))

        cursor.reset().moveBy(0, size)

        if (items != null) {
            for (item in items.reversed()) {
                val jiraCursor = Cursor(cursor)
                drawRect(jiraCursor.moveBy(0, size / -10).coord, size, size / 10, item.status.color)
                jiraCursor.moveBy((size / 10 * 0.3).toInt(), (size / 10 * 0.7).toInt())
                s.text(jiraCursor.x, jiraCursor.y, item.desc).attr(json("font-size" to "${size / 14}px"))
                if (item.by != null) s.text(jiraCursor.x + (size / 10 * 7.1), jiraCursor.y, item.by).attr(json("font-size" to "${size / 20}px"))
                if (item.jira != null) {
                    val jiraLink = s.text(jiraCursor.x + size, jiraCursor.y, "J").attr(json("fill" to "orange", "cursor" to "pointer"))
                    jiraLink.click({ window.open("https://somejira.com/browse/ABCD-${item.jira}", "_blank") })
                }
                cursor.moveBy(0, size / -10)
            }
        }
    }
}

data class Coord(val x: Int, val y: Int)

data class Cursor(var coord: Coord) {
    val original = coord

    val x: Int
        get() = coord.x
    val y: Int
        get() = coord.y

    constructor(cursor: Cursor) : this(cursor.coord)

    fun moveBy(xMove: Int, yMove: Int): Cursor {
        coord = Coord(coord.x + xMove, coord.y + yMove)
        return this
    }

    fun reset(): Cursor {
        coord = original
        return this
    }
}

enum class WorkStatus(val color: String) {
    NEW("white"), IN_PROGRESS("yellow"), LATE("brown"), VERY_LATE("red"), DONE("lightgreen")
}

data class WorkItem(val desc: String, val status: WorkStatus, val by: String? = null, val jira: Int? = null)
